#!/usr/bin/env python

def main():
    a = Node("a")
    b = Node("b")
    c = Node("c")
    d = Node("d")
    e = Node("e")
    f = Node("f")
    g = Node("g")
    h = Node("h")
    i = Node("i")
    a.add_edge(b, 4)
    a.add_edge(g, 8)
    b.add_edge(c, 3)
    b.add_edge(g, 14)
    c.add_edge(d, 7)
    c.add_edge(e, 2)
    c.add_edge(i, 13)
    d.add_edge(e, 11)
    d.add_edge(f, 9)
    d.add_edge(i, 12)
    e.add_edge(g, 7)
    e.add_edge(h, 6)
    f.add_edge(i, 10)
    g.add_edge(h, 1)
    h.add_edge(i, 2)
    nodes = [a, b, c, d, e, f, g, h, i]
    g = Graph(nodes)
    print "Edges for the MST: "
    mst = Kruskal(g)
    print

    print "Prim: "
    prim(g, a)

    print
    clrs()

def clrs():
    print "Computing solution for CLRS p.632"
    a = Node("a")
    b = Node("b")
    c = Node("c")
    d = Node("d")
    e = Node("e")
    f = Node("f")
    g = Node("g")
    h = Node("h")
    i = Node("i")
    a.add_edge(b, 4)
    a.add_edge(h, 8)
    b.add_edge(c, 8)
    b.add_edge(h, 11)
    c.add_edge(d, 7)
    c.add_edge(f, 4)
    c.add_edge(i, 2)
    d.add_edge(e, 9)
    d.add_edge(f, 14)
    e.add_edge(f, 10)
    f.add_edge(g, 2)
    g.add_edge(h, 1)
    g.add_edge(i, 6)
    h.add_edge(i, 7)
    nodes = [a, b, c, d, e, f, g, h, i]
    g1 = Graph(nodes)
    print "Edges for MST: "
    mst = Kruskal(g1)
    print
    print "CLRS p.635"
    prim(g1, a)

def Kruskal(g):
    # Master set
    S = []
    MST = []
    for v in g.nodes:
        S.append( [v, ] )
    # Sort edges by weight
    g.edges = sorted(g.edges, key = lambda e: e[2] )
    for edge in g.edges:
        u = edge[0]
        v = edge[1]
        if find_set(u, S) != find_set(v, S):
            MST.append( [u, v, edge[2]] )
            union(u, v, S)
            print "+ {0}".format( [u, v, edge[2]] )
        else:
            print "\t Skip: {0}".format([u, v, edge[2]])
    return MST

def prim(g, r):
    for v in g.nodes:
        v.key = float("inf")
        v.pi = None
    r.key = 0
    Q = []
    for n in g.nodes:
        Q.append(n)

    while len(Q) > 0:
        u = min(Q, key = lambda n: n.key)
        Q.remove(u)
        for (v, w) in g.adj(u):
            if v in Q and w < v.key:
                v.pi = u
                v.key = w
        A = []
        for elem in g.nodes:
            if elem not in Q:
                A.append( (elem, elem.pi) )
        print A

def union(x, y, S):
    sx = None
    sy = None

    for a_set in S:
        if x in a_set:
            sx = a_set
        elif y in a_set:
            sy = a_set

    S.remove(sx)
    S.remove(sy)
    sx.extend(sy)
    S.append(sx)

def find_set(x, master_set):
    for s in master_set:
        if x in s:
            return s[0]
    

class Graph(object):
    def __init__(self, nodes=[]):
        self.nodes = nodes
        # 3-uples of n1, n2, weight
        self.edges = []
        for n in self.nodes:
            self.add_edges(n)

    def add_vertex(self, n):
        self.nodes.append(n)
        self.add_edges(n)

    def print_graph(self):
        for n in self.nodes:
            n.print_node()

    def add_edges(self, vertex):
        for e in vertex.edges:
            if e not in self.edges:
                self.edges.append(e)

    def adj(self, n):
        # Retourne une liste de 2-uple de la forme:
        # (voisin, distance)
        buddies = []
        for e in self.edges:
            if e[0] == n:
                if e[1] not in buddies:
                    buddies.append( (e[1], e[2]) )
            elif e[1] == n:
                if e[0] not in buddies:
                    buddies.append( (e[0], e[2]) )
        return buddies


class Node(object):
    def __init__(self, val):
        self.value = val
        self.edges = []

    def add_edge(self, n2, weight):
        e = (self, n2, weight)
        self.edges.append( e )
        n2.edges.append( e ) 

    def print_node(self):
        print "[{0}]".format(self.value)
        for edge in self.edges:
            print "\t[{0}] ({1})".format(edge[0], edge[2])

    def __repr__(self):
        return self.value.__repr__()

if __name__ == '__main__':
    main()
